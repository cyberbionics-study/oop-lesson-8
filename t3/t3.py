import pickle


products = [
    "Smartphone",
    "Laptop",
    "Wireless Headphones",
    "Smart Watch",
    "Digital Camera",
    "Tablet",
    "Gaming Console",
    "Fitness Tracker",
    "Bluetooth Speaker",
    "Noise-Canceling Headphones",
    "Coffee Maker",
    "Portable Power Bank",
    "LED TV",
    "Wireless Mouse",
    "External Hard Drive",
    "Gaming Mouse",
    "Fitness Mat",
    "Desk Chair",
    "Cookware Set",
    "Robot Vacuum Cleaner",
    "Electric Toothbrush",
    "Air Purifier",
    "Wireless Router",
    "Sneakers",
    "Backpack"
]

with open('online store products list.json', 'wb') as file:
    pickle.dump(products, file)

with open('online store products list.json', 'rb') as file:
    loaded_data = pickle.load(file)

print(loaded_data)
