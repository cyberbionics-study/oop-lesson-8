import shelve


def write_dict_file(file_name):
    """Write dict in file"""
    print('Fill out the link shortener database.\n'
          'to close enter "1"')
    with shelve.open(file_name) as link:
        while True:
            dict_key = input('Shortened link: ').strip().lower()
            if dict_key == '1':
                print('-' * 50)
                break
            dict_value = input('Link: ').strip().lower()
            link[dict_key] = dict_value


def open_dict_file(file_name):
    """Open dict file and search"""
    print('', 'Searching links by shortened form.\n'
              ' to close enter "1"\n'
          'to open adding menu again enter "2"')
    with shelve.open(file_name) as link:
        while True:
            short_link = input('Shortened link: ').strip().lower()
            if short_link == '1':
                print('-' * 50)
                break
            if short_link == '2':
                print('-' * 50)
                write_dict_file(file_name)
            elif short_link in link:
                print(f'{short_link}: {link[short_link]}')
            else:
                print(f'Shortened link: {short_link} does not exist.')


if __name__ == '__main__':
    name = "t2.txt"
    write_dict_file(name)
    open_dict_file(name)
