import random


with open('10k_random.txt', 'w') as file:
    for i in range(10000):
        random_num = round(random.uniform(0, 10000))
        file.write(str(random_num) + ', ')
